import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import 'jest-dom/extend-expect'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import Search from './Search'

function renderWithRouter(
  ui,
  {
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] })
  } = {}
) {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    history
  }
}

const { getByText, getByPlaceholderText } = renderWithRouter(<Search />)

test('<Input />', () => {
  const SearchInput = getByPlaceholderText('employee name')
  expect(SearchInput).toBeVisible()

  fireEvent.change(SearchInput, { target: { value: 'john' } })
  expect(SearchInput.value).toBe('john')
})

test('<Button />', () => {
  const Button = getByText('Search')
  expect(Button).toBeVisible()
})
