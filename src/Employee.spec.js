import React from 'react'
import { render, waitForElement } from '@testing-library/react'
import 'jest-dom/extend-expect'
import axios from 'axios'
import Employee, { removeDuplicatedSubordintates } from './Employee'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'

afterEach(() => {
  axios.post.mockClear()
})

function mockCall(direct_subordinates) {
  axios.post.mockResolvedValueOnce({
    data: {
      position: 'ceo',
      direct_subordinates
    }
  })
}

function renderWithRouter(
  ui,
  {
    route = '/overview/mark',
    history = createMemoryHistory({ initialEntries: [route] })
  } = {}
) {
  return {
    ...render(
      <Router
        history={history}
        match={{
          params: {
            name: 'mark'
          }
        }}
      >
        {ui}
      </Router>
    ),
    history
  }
}

test('show no direct_subordinate msg', async () => {
  mockCall([])

  const { getByTestId } = renderWithRouter(<Employee />)
  expect(axios.post).toHaveBeenCalledTimes(1)

  const noSubordinate = await waitForElement(() =>
    getByTestId('no_subordinate')
  )
  expect(noSubordinate).toHaveTextContent('No Subordinate')
})

test('remove duplicated direct_subordinates between arrays', () => {
  const data = [
    [
      {
        name: 'Samad Pitt',
        direct_subordinates: ['Aila Hodgson', 'Amaya Knight']
      }
    ],
    [
      {
        name: 'Leanna Hogg',
        direct_subordinates: [
          'Vincent Todd',
          'Faye Oneill',
          'Lynn Haigh',
          'Aila Hodgson'
        ]
      }
    ]
  ]

  const result = removeDuplicatedSubordintates(data)
  expect(result).toEqual([
    [
      {
        name: 'Samad Pitt',
        direct_subordinates: ['Aila Hodgson', 'Amaya Knight']
      }
    ],
    [
      {
        name: 'Leanna Hogg',
        direct_subordinates: ['Vincent Todd', 'Faye Oneill', 'Lynn Haigh']
      }
    ]
  ])
})
