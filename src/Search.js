import React, { useState, useRef } from 'react'
import { Button, Box } from 'react-bulma-components'
import styled from 'styled-components'
import {
  Field,
  Label,
  Control as ControlBare,
  Input
} from 'react-bulma-components/lib/components/form'

import { withRouter } from 'react-router-dom'
import useOnClickOutside from './useOnClickOutside'

const Control = styled(ControlBare)`
  display: flex;
  button {
    margin-left: 10px;
  }
`

const HistoryWrap = styled.div`
  position: absolute;
  top: 37px;
  left: 0;
  width: 81%;
`

const HistoryItem = styled.div`
  display: flex;
  justify-content: space-between;
  span {
    cursor: pointer;
    &:hover {
      text-decoration: underline;
    }
  }
`

const Delete = styled.div`
  cursor: pointer;
`

function Search(props) {
  const [searchTerm, setSearchTerm] = useState('')
  const [history, setHistory] = useState([])
  const [showHistory, toggleHistory] = useState(false)

  const ref = useRef()
  useOnClickOutside(ref, () => toggleHistory(false))

  function handleOnFocus() {
    toggleHistory(true)

    const history = localStorage.getItem('search_history')
    setHistory(JSON.parse(history))
  }

  function handleSearch() {
    if (!searchTerm) {
      return false
    }

    const history = localStorage.getItem('search_history')
    const parsedHistory = JSON.parse(history) || []

    const searchTermExisted = parsedHistory.length
      ? parsedHistory.find(o => o.name === searchTerm)
      : false
    const maxId = parsedHistory.length
      ? Math.max(...parsedHistory.map(o => o.id))
      : 0

    if (searchTermExisted) {
      //move name to the top
      const toSaveArray = [
        {
          id: maxId + 1,
          name: searchTerm
        },
        ...parsedHistory.filter(o => o.name !== searchTerm)
      ]

      localStorage.setItem('search_history', JSON.stringify(toSaveArray))
    } else {
      //save new record to history
      localStorage.setItem(
        'search_history',
        JSON.stringify([
          {
            id: maxId + 1,
            name: searchTerm
          },
          ...parsedHistory
        ])
      )
    }

    searchTerm && props.history.push(`/overview/${searchTerm}`)
  }

  function deleteHistory(id) {
    const history = localStorage.getItem('search_history')
    const parsedHistory = JSON.parse(history) || []

    const toSaveArray = parsedHistory.filter(o => o.id !== id)
    setHistory(toSaveArray)

    localStorage.setItem('search_history', JSON.stringify(toSaveArray))
  }

  return (
    <Box>
      <Field>
        <Label>Search Employee</Label>
        <Control>
          <Input
            value={searchTerm}
            onChange={e => setSearchTerm(e.target.value)}
            placeholder='employee name'
            data-testid='search_input'
            onClick={handleOnFocus}
          />
          {history && history.length > 0 && showHistory && (
            <HistoryWrap ref={ref}>
              <Box>
                {history.map(o => (
                  <HistoryItem key={o.id}>
                    <span
                      onClick={() => {
                        setSearchTerm(o.name)
                        toggleHistory(false)
                      }}
                    >
                      {o.name}
                    </span>
                    <Delete onClick={() => deleteHistory(o.id)}>✕</Delete>
                  </HistoryItem>
                ))}
              </Box>
            </HistoryWrap>
          )}
          <Button onClick={handleSearch} color='primary'>
            Search
          </Button>
        </Control>
      </Field>
    </Box>
  )
}

export default withRouter(Search)
