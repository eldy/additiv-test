import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import { withRouter } from 'react-router-dom'
import { Box, Heading } from 'react-bulma-components'

const EmployeeName = styled(Heading)`
  &&& {
    text-transform: capitalize;
  }
`

export const API = 'http://localhost:3001'

export function removeDuplicatedSubordintates(result) {
  const set = new Set()
  const filteredArrs = result.map(o =>
    o.map(o => ({
      name: o.name,
      direct_subordinates: o.direct_subordinates
        ? o.direct_subordinates.filter(o2 => {
            const has = set.has(o2)
            set.add(o2)
            return !has
          })
        : []
    }))
  )

  return filteredArrs
}

function Employee(props) {
  const name = props.match.params.name
  const [fetching, setFetching] = useState(true)
  const [data, setData] = useState({
    position: '',
    direct_subordinates: ''
  })
  const [error, setError] = useState('')

  useEffect(() => {
    async function searchEmployee({ name }) {
      const {
        data,
        data: { position, direct_subordinates }
      } = await axios.post(`${API}`, { name })

      if (data.error) {
        setError('Employee not found')
      }

      if (direct_subordinates) {
        //fetch nested subordintates
        const result = await Promise.all(
          direct_subordinates.map(async name => {
            const { data } = await axios.post(`${API}`, { name })
            return [
              {
                name: data.name,
                direct_subordinates: data.direct_subordinates
              }
            ]
          })
        )

        setData({
          position,
          direct_subordinates: removeDuplicatedSubordintates(result)
        })
      } else {
        setData({
          position,
          direct_subordinates: []
        })
      }

      setFetching(false)
    }

    searchEmployee({ name })
  }, [name])

  return (
    <Box>
      {fetching ? (
        'Loading...'
      ) : error ? (
        error
      ) : (
        <div>
          <EmployeeName>{name}</EmployeeName>
          <Heading subtitle>
            Position: <span>{data.position}</span>
          </Heading>
          {data && data.direct_subordinates.length > 0 ? (
            <div>
              <Heading style={{ marginBottom: 0 }} subtitle>
                Subordinates
              </Heading>
              {data.direct_subordinates.map(v =>
                v.map(o => (
                  <div key={v}>
                    {o.name}
                    <div style={{ marginLeft: '10px' }}>
                      {o.direct_subordinates.map(o2 => o2).join(', ')}
                    </div>
                  </div>
                ))
              )}
            </div>
          ) : (
            <div data-testid='no_subordinate'>No Subordinate</div>
          )}
        </div>
      )}
    </Box>
  )
}

export default withRouter(Employee)
