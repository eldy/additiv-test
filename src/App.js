import React from 'react'
import styled from 'styled-components'
import { Container as ContainerBare, Columns } from 'react-bulma-components'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Search from './Search'
import Employee from './Employee'

const Container = styled(ContainerBare)`
  margin-top: 20px;
  max-width: 500px;
`

function App() {
  return (
    <Container>
      <Columns.Column>
        <Router>
          <Container>
            <Route exact path='/' component={Search} />
            <Route exact path='/overview/:name' component={Employee} />
          </Container>
        </Router>
      </Columns.Column>
    </Container>
  )
}

export default App
