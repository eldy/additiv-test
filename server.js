const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const axios = require('axios')

const app = express()
app.use(cors())

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const API = 'http://api.additivasia.io/api/v1/assignment/employees'

app.post(`/`, async (req, res) => {
  const { name } = req.body

  try {
    const {
      data,
      data: [position, subordinates]
    } = await axios.get(`${API}/${name}`)
    console.log('data:', data)

    res.json({
      name,
      position,
      direct_subordinates: subordinates
        ? subordinates['direct-subordinates']
        : []
    })
  } catch (err) {
    console.log(err)
    res.json({
      error: 'Invalid request'
    })
  }
})

app.listen('3001', () => {
  console.log('server running at port 3001')
})
